import * as nodemailer from "nodemailer"
import { CONFIG_MAIL_HOST, CONFIG_MAIL_PASSWORD, CONFIG_MAIL_SECURE, CONFIG_MAIL_USER, CONFIG_MAIL_REQUIRE_TLS, CONFIG_MAIL_TLS_REJECT_UNAUTHORIZED } from "./config"

export async function createMailer(): Promise<nodemailer.Transporter> {
  return nodemailer.createTransport({
    host: CONFIG_MAIL_HOST(),
    port: 587,
    secure: CONFIG_MAIL_SECURE(),
    auth: {
      user: CONFIG_MAIL_USER(),
      pass: CONFIG_MAIL_PASSWORD()
    },
    requireTLS: CONFIG_MAIL_REQUIRE_TLS(),
    tls: {
      rejectUnauthorized: CONFIG_MAIL_TLS_REJECT_UNAUTHORIZED()
    }
  });
}
