import * as fs from 'fs'
import * as path from 'path'
import { promisify } from 'util'
import moment from "moment-timezone";
import { SvjUnit } from "./public-api/model/svjUnit";
import { SvjMeter } from './public-api/model/svjMeter';
import { SvjWaterMeterPoint } from './public-api/model/svjWaterMeterPoint';
import { SvjUnitStats } from './public-api/model/svjUnitStats';
import { fatal, isNotUndefined } from './utility';

const readdir = promisify(fs.readdir)
const readFile = promisify(fs.readFile)

export interface StorageAccount {
  accountId: string;
  emailAddress: string;
  name: string;
  unitNos: number[];
}

export type StorageUnit = SvjUnit

export type StorageMeter = SvjMeter

export type StorageWaterMeterPoint = SvjWaterMeterPoint

export interface Storage {
  refresh(): Promise<void>;
  loadAccountByEmailAddressAndUnitNo(emailAddress: string, unitNo: number): Promise<StorageAccount | undefined>;
  loadAccountByAccountId(accountId: string): Promise<StorageAccount | undefined>;
  loadUnitStats(unitNos: number[]): Promise<SvjUnitStats[]>;
}

interface FileStorageState {
  accounts: StorageAccount[];
  units: StorageUnit[];
  meters: StorageMeter[];
  points: StorageWaterMeterPoint[];
}

export async function createFileStorage(
  watermeterExportsPath: string,
): Promise<Storage> {
  let state = await loadState();

  return {
    refresh,
    loadAccountByEmailAddressAndUnitNo,
    loadAccountByAccountId,
    loadUnitStats,
  }

  async function refresh() {
    state = await loadState();
  }

  async function loadAccountByEmailAddressAndUnitNo(emailAddress: string, unitNo: number): Promise<StorageAccount | undefined> {
    return state.accounts.find(a => a.emailAddress === emailAddress && a.unitNos.indexOf(unitNo) > -1);
  }

  async function loadAccountByAccountId(accountId: string): Promise<StorageAccount | undefined> {
    return state.accounts.find(a => a.accountId === accountId);
  }

  async function loadUnitStats(unitNos: number[]): Promise<SvjUnitStats[]> {
    return state
      .units
      .filter(u => unitNos.indexOf(u.unitNo) > -1)
      .map(unit => ({
        unit,
        meterSerials: new Set(state.meters.filter(m => m.unitNo === unit.unitNo).map(m => m.serialNumber)),
      }))
      .map(unit => ({
        unit: unit.unit,
        meters: state.meters.filter(m => unit.meterSerials.has(m.serialNumber)),
        points: state.points.filter(p => unit.meterSerials.has(p.serialNumber))
      }))
  }

  async function loadDatabase(): Promise<FileStorageState> {
    const data = await readFile(path.join(watermeterExportsPath, "data.json"), { encoding: 'utf-8' })
    return JSON.parse(data);
  }

  async function loadWatermeterPoints(): Promise<StorageWaterMeterPoint[]> {
    const CSV_PATTERN = /^(\d{4})-(\d{2})-(\d{2})\.csv$/;

    const files = await readdir(watermeterExportsPath, { withFileTypes: true });
    const csvFiles = files.filter(de => de.isFile() && CSV_PATTERN.test(de.name)).map(de => de.name)
    csvFiles.sort();
    const csvContents = await Promise.all(csvFiles.map(csvFile => readFile(path.join(watermeterExportsPath, csvFile))))
    const csvMergedContents = csvContents.join('\n');
    const csv = csvMergedContents.split(/\r?\n/).filter(e => e.trim().length).map(line => line.split(/;/));
    const values = Array.from(csv
      .filter(csvLine => parseAndFixDate(csvLine[1]))
      .map(csvLine => ({
        serialNumber: csvLine[0],
        date: parseAndFixDate(csvLine[1]) || fatal("should be filtered"),
        originalDate: csvLine[1],
        volume: parseFloat(csvLine[11]),
        hours: parseInt(csvLine[13], 10),
        errorCode: parseInt(csvLine[27], 10),
      }))
      .reduce((map, item) => map.set(`${item.date}::${item.serialNumber}`, item), new Map<string, StorageWaterMeterPoint>())
      .values())
    values.sort((a, b) => a.serialNumber < b.serialNumber ? -1 : a.serialNumber > b.serialNumber ? 1 : a.date < b.date ? -1 : a.date > b.date ? 1 : 0)
    return values

    function parseAndFixDate(date: string): string | undefined {
      let d = moment(date, "DDMMYYYY HHmm", true)
      if (!d.isValid()) {
        return undefined;
      }
      if (d.hour() * 60 + d.minutes() < 2 * 60 + 10) {
        d = d.startOf('day').subtract(5, 'minutes')
      }
      return d.format("YYYY-MM-DD");
    }
  }

  async function loadState(): Promise<FileStorageState> {
    const database = await loadDatabase();
    const watermeterPoints = await loadWatermeterPoints();
    return {
      ...database,
      points: watermeterPoints
    }
  }
}
