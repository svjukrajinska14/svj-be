import { spawn } from "child_process";
import * as crypto from "crypto";
import express from "express";
import { Readable, Writable } from "stream";
import * as tmp from "tmp";
import { PARENT_LOGGER } from "./logger";

const logger = PARENT_LOGGER.child({ module: "utility" });

export const MILLISECONDS_IN_HOUR = 60 * 60 * 1000;
export const MILLISECONDS_IN_MINUTE = 60 * 1000;

export type PartialOrNull<T> = {
  [P in keyof T]?: T[P] | null;
};

// Type that lists keys of a type T that have specific type S
export type KeysOfType<T, S> = { [k in keyof T]: T[k] extends S ? k : never }[keyof T];

// Type that only contains those properties of type T, that have specific type S
export type OnlyPropertiesOfType<T, S> = { [k in KeysOfType<T, S>]: S };

export interface ExpressHandlerResponse<T> {
  status: number;
  body: T;
}

export interface BaseParams {
  [name: string]: string;
}

export interface Closeable {
  close(): Promise<void>;
}

export type ExpressHandler<T extends BaseParams> = (
  req: express.Request<T>,
  res: express.Response,
  next: express.NextFunction
) => Promise<void>;
export type EndpointHandler<T extends BaseParams, U> = (req: express.Request<T>) => Promise<ExpressHandlerResponse<U>>;

export function removeUndefinedProperties<T>(obj: T): T {
  Object.keys(obj).forEach((key) => obj[key as keyof T] === undefined && delete obj[key as keyof T]);
  return obj;
}

export function wrapExpressHandler<T extends BaseParams, U>(handler: EndpointHandler<T, U>): ExpressHandler<T> {
  return async function (req: express.Request<T>, res: express.Response, next: express.NextFunction) {
    try {
      const result = await handler(req);
      res.status(result.status || 200);
      if (result.body) {
        res.json(result.body);
      }
      res.end();
    } catch (e) {
      next(e);
    }
  };
}

export function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export class ApplicationError extends Error {
  status: number;
  response?: unknown;

  constructor(message: string, status: number, response?: unknown) {
    super(message);
    this.status = status;
    this.response = response;
  }
}

export class ExactlyOnePropertyCanBeSetError extends ApplicationError {
  constructor(properties: string[]) {
    super(`Exactly one of properties ` + properties.join(", ") + ` can be set at the same time`, 400);
  }
}

export class TimeoutException extends ApplicationError {
  constructor() {
    super(`Timeout processing request`, 500);
  }
}

export class AccessDeniedError extends ApplicationError {
  constructor(reason: string) {
    super(`Access denied: ${reason}`, 403);
  }
}

export class InvalidRequestError extends ApplicationError {
  constructor(message: string) {
    super(message, 400);
  }
}

export function consumeReadable(stream: Readable): Promise<Buffer> {
  return new Promise((resolve) => {
    const buffers: Buffer[] = [];
    stream.on("data", (b) => buffers.push(b));
    stream.on("end", () => {
      resolve(Buffer.concat(buffers));
    });
  });
}

export async function copyWholeStream(is: Readable, os: Writable): Promise<void> {
  return new Promise((resolve, reject) => {
    is.on("end", () => resolve());
    is.on("error", (e) => reject(e));
    is.pipe(os, { end: true });
  });
}

export function assertExactlyOneOfPropertiesIsSet<T>(obj: T, ...properties: (keyof T)[]): void {
  const numberOfPropertiesSet = properties.map((p) => p in obj).filter((t) => t).length;
  if (numberOfPropertiesSet != 1) {
    throw new ExactlyOnePropertyCanBeSetError(properties.map((p) => "" + p));
  }
}

export function arraysEqual<T>(a: T[], b: T[]): boolean {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  for (let i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

export function executeExternalFilter<T>(
  command: string,
  args: string[],
  input: Readable | undefined,
  consumer: (r: Readable) => Promise<T>
): Promise<T> {
  const startTs = process.hrtime();
  return new Promise((resolve, reject) => {
    const prc = spawn(command, args);
    if (input) {
      input.pipe(prc.stdin);
    }
    prc.stderr.on("data", (data) => {
      logger.error(`External filter STDERR ${data}`);
    });
    prc.on("close", function (code) {
      if (code !== 0) {
        const error = `Process "${command} ${args.join(" ")}" exitted with code ${code}`;
        logger.error(error);
        reject(new Error(error));
      }
      // do not resolve if the close was a success
    });
    prc.on("error", function (e) {
      reject(e);
    });
    consumer(prc.stdout)
      .then((v) => {
        const hrTime = process.hrtime(startTs);
        const millis = Math.round(hrTime[0] * 1000000 + hrTime[1] / 1000) / 1000;
        logger.child({ command, args, millis }).trace(`Execution of ${command} took ${millis}ms`);
        resolve(v);
      })
      .catch(reject);
  });
}

export function createOtp(data: string): string {
  const bfr = crypto.createHash("sha256").update(`${Date.now()}${data}`).digest();
  const pin = String(bfr.reduce((acc, i) => acc + i, 0) % 10000);
  return "0000".substr(0, 4 - pin.length) + pin;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
export async function closeOne(closeable?: any): Promise<void> {
  try {
    if (!closeable) {
      return;
    }
    if (typeof closeable.close === "function") {
      await closeable.close();
      return;
    }
    if (typeof closeable.stop === "function" && closeable.running) {
      closeable.stop();
      if (closeable.running) {
        logger.info(`Awaiting jobs completion...`);
        while (closeable.running) {
          await sleep(1000);
        }
      }
      return;
    }
  } catch (e) {
    logger.error(`Error closing ${closeable}: ${e}`);
  }
}

export async function withTemporaryFile<T>(fn: (path: string) => Promise<T>, options?: tmp.FileOptions): Promise<T> {
  const temporaryFile = await new Promise<{ path: string; cleanupCallback: () => void }>((resolve, reject) => {
    tmp.file({ mode: 0o600, prefix: "hobit-", ...options }, (err, path, _fd, cleanupCallback) => {
      if (err) {
        reject(err);
        return;
      }
      resolve({ path, cleanupCallback });
    });
  });

  try {
    return await fn(temporaryFile.path);
  } finally {
    temporaryFile.cleanupCallback();
  }
}

export function fatal(reason: string): never {
  logger.error(`FATAL: ${reason}`);
  throw new Error(`FATAL ERROR: ${reason}`);
}

export function deepCopy<T>(a: T): T;
export function deepCopy<T>(a: T | undefined): T | undefined {
  if (!a) {
    return undefined;
  }
  return JSON.parse(JSON.stringify(a));
}

export interface WalkNodeCallbackFunctions<R> {
  readonly root: R;
  readonly path: string;
  replace: (value: unknown) => void;
  break: () => void;
  exit: () => void;
}

export interface WalkNodeCallback<R> {
  (attr: string | number | undefined, value: unknown, primitive: boolean, context: WalkNodeCallbackFunctions<R>):
    | undefined
    | void
    | boolean;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
export function walkJson<R>(root: R, fn: WalkNodeCallback<R>): void {
  recurse(undefined, root, { root, path: "", replace: () => void 0, break: () => void 0, exit: () => void 0 });

  type RecurseResult = "CONTINUE" | "BREAK" | "EXIT";

  // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
  function recurse(attr: string | number | undefined, o: unknown, context: WalkNodeCallbackFunctions<R>): void {
    switch (typeof o) {
      case "undefined":
      case "string":
      case "number":
      case "boolean":
      case "symbol":
      case "bigint":
        fn(attr, o, true, context);
        return;
      case "function":
        // we do not walk functions
        return;
    }

    // it is definitelly an object now
    if (o === null) {
      fn(attr, o, true, context);
      return;
    }

    if (fn(attr, o, false, context) === false) {
      return;
    }

    if (Array.isArray(o)) {
      let result: RecurseResult = "CONTINUE";

      const array = o as Array<unknown>;
      for (let idx = 0; idx < array.length; idx++) {
        recurse(idx, array[idx], {
          root: context.root,
          path: `${context.path}.${idx}`,
          replace: (value: unknown) => {
            array[idx] = value;
          },
          break: () => {
            result = "BREAK";
          },
          exit: () => {
            result = "EXIT";
          },
        });

        if (result === "CONTINUE") {
          continue;
        }
        if (result === "EXIT") {
          context.exit();
        }
        break;
      }
      return;
    }

    if (typeof o === "object") {
      let result: RecurseResult = "CONTINUE";

      const obj = o as Record<string, unknown>;
      for (const key of Object.keys(obj).sort()) {
        recurse(key, obj[key], {
          root: context.root,
          path: `${context.path}.${key}`,
          replace: (value: unknown) => {
            obj[key] = value;
          },
          break: () => {
            result = "BREAK";
          },
          exit: () => {
            result = "EXIT";
          },
        });

        if (result === "CONTINUE") {
          continue;
        }
        if (result === "EXIT") {
          context.exit();
        }
        break;
      }
      return;
    }

    throw new Error(`Unknown object type ${o}`);
  }
}

export function joinSentence(intermediateJoiner: string, finalJoiner: string, elements: string[]): string {
  return elements.map((e, i) => (i === 0 ? "" : i === elements.length - 1 ? finalJoiner : intermediateJoiner) + e).join("");
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
export function expressMaximumLengthChecker(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  maxStringLength: number,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  maxArrayLength: number
): ExpressHandler<BaseParams> {
  return async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    // if (typeof req.query === 'object') {
    //   if (!checkMaximumLengths(maxStringLength, maxArrayLength, req.query)) {
    //     next(new InvalidRequestError('Request item too long'));
    //     return;
    //   }
    // }
    // if (typeof req.params === 'object') {
    //   if (!checkMaximumLengths(maxStringLength, maxArrayLength, req.params)) {
    //     next(new InvalidRequestError('Request item too long'));
    //     return;
    //   }
    // }
    // if (!req.body || req.body instanceof Buffer) {
    //   next();
    //   return;
    // }
    // if (typeof req.body === 'object') {
    //   if (!checkMaximumLengths(maxStringLength, maxArrayLength, req.body)) {
    //     next(new InvalidRequestError('Request item too long'));
    //     return;
    //   }
    // } else if (typeof req.body === 'string') {
    //   if (req.body.length > maxStringLength) {
    //     next(new InvalidRequestError('Request item too long'));
    //     return;
    //   }
    // }
    next();
  };
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
// export function checkMaximumLengths(maxStringLength: number, maxArrayLength: number, root: any): boolean {
//   return walkJson(root, (_attr, value) => {
//     if (value) {
//       if (typeof value === 'string') {
//         if (value.length > maxStringLength) {
//           return false;
//         }
//       } else if (Array.isArray(value)) {
//         if (value.length > maxArrayLength) {
//           return false;
//         }
//       }
//     }
//     return true;
//   });
// }

// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
export function extraDollarAttributesRemover(): ExpressHandler<any> {
  return async (req, _res, next) => {
    if (req.body && typeof req.body === "object" && "$" in req.body) {
      delete req.body.$;
    }
    next();
  };
}

export interface RoutesConfigurer {
  configureRoutes(router: express.Router): Promise<void>;
}

export function escapeHtml(text: string): string {
  return text.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
}

export function objectChecksum(obj: unknown): string {
  const hash = crypto.createHash("sha1");
  walkJson(obj, (attr, value, primitive) => {
    if (typeof attr !== "undefined") {
      hash.update(String(attr));
    }
    if (primitive) {
      hash.update(String(value));
    }
  });
  return hash.digest().toString("base64");
}

export function createId(namespace: string, source: string): string;
export function createId(namespace: string, source: undefined): undefined;
export function createId(namespace: string, source: string | undefined): string | undefined;
export function createId(namespace: string, source: string | undefined): string | undefined {
  if (typeof source === "undefined") {
    return;
  }
  return crypto.createHash("sha256").update(`${namespace}::${source}`).digest("hex").substring(0, 63);
}

export function isNotUndefined<T>(a: T | undefined): a is T {
  return typeof a !== 'undefined';
}
