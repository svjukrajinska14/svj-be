import http from "http";
import { CONFIG_PORT, CONFIG_WATERMETER_EXPORTS_PATH } from "./config";
import { PARENT_LOGGER } from "./logger";
import { createApplication } from "./server";
import { createFileStorage } from "./storage";

const logger = PARENT_LOGGER.child({ module: "index" });

async function run() {
  const storage = await createFileStorage(CONFIG_WATERMETER_EXPORTS_PATH());

  const svjApp = await createApplication({
    storage,
  });
  const app = svjApp.app;
  const httpServer = http.createServer(app);

  const port = CONFIG_PORT();
  httpServer.listen(port, () => {
    logger.info(`Serving on :${port}`);
  });
}

run();
