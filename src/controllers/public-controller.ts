import express from "express";
import * as OpenApiValidator from "express-openapi-validator";
import fs from "fs";
import * as jsyaml from "js-yaml";
import path from "path";
import swaggerUi from "swagger-ui-express";
import { SvjUnitStats } from "../public-api/model/svjUnitStats";
import { Storage } from "../storage";
import { BaseParams, ExpressHandlerResponse, RoutesConfigurer, wrapExpressHandler } from "../utility";
import { AuthenticationController } from "./authentication-controller";

export async function createPublicController(
  storage: Storage,
  authenticationController: AuthenticationController,
): Promise<RoutesConfigurer> {
  return {
    async configureRoutes(app: express.Router): Promise<void> {
      app.use("/api/public/v1", await createPublicRouterV1());
    },
  };

  async function createPublicRouterV1(): Promise<express.Router> {
    const router = express.Router();
    const apiSpecPath = path.join(__dirname, "..", "..", "interfaces", "public.openapi.yml");
    const apiSpec = jsyaml.load(fs.readFileSync(apiSpecPath, "utf-8")) as swaggerUi.JsonObject;

    router.use("/api-docs", swaggerUi.serve);
    router.get("/api-docs/swagger.yml", (_req: express.Request, res: express.Response) =>
      res.status(200).type("application/x-yaml").send(jsyaml.dump(apiSpec)).end()
    );
    router.get("/api-docs", (req: express.Request, res: express.Response, next: express.NextFunction) =>
      swaggerUi.setup(apiSpec, { explorer: true })(req, res, next)
    );

    router.use(authenticationController.authenticateSvjUser);
    router.use(
      OpenApiValidator.middleware({
        apiSpec: apiSpecPath,
        validateResponses: true,
        $refParser: {
          mode: "dereference",
        },
      })
    );

    router.get("/stats", wrapExpressHandler(getUnitStats));

    return router;
  }

  async function getUnitStats(req: express.Request<BaseParams>): Promise<ExpressHandlerResponse<SvjUnitStats[]>> {
    const units = await storage.loadUnitStats(req.account.unitNos);

    return {
      status: 200,
      body: units,
    };
  }
}
