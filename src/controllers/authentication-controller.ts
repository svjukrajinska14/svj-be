import express from "express";
import { contextSet } from "../logger";
import { Storage } from "../storage";
import * as jwt from 'jsonwebtoken';
import { AccessDeniedError, BaseParams, ExpressHandlerResponse, InvalidRequestError, RoutesConfigurer, wrapExpressHandler } from "../utility";
import { CONFIG_EMAIL_OTP_TIMEOUT_MINUTES, CONFIG_JWT_SECRET, CONFIG_MAIL_FROM, CONFIG_MAIL_OTP_URL, CONFIG_SESSION_TIMEOUT_MINUTES } from "../config";
import { Transporter } from "nodemailer";

// const logger = PARENT_LOGGER.child({ module: "authentication-controller" });

export interface AuthenticationController {
  authenticateSvjUser(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void>;
}

export async function createAuthenticationController(
  storage: Storage,
  mailer: Transporter,
): Promise<AuthenticationController & RoutesConfigurer> {

  return {
    authenticateSvjUser,
    async configureRoutes(app: express.Router): Promise<void> {
      app.use("/api/auth/v1", await createAuthRouterV1());
    },
  };

  async function createAuthRouterV1(): Promise<express.Router> {
    const router = express.Router();
    router.post("/otp-start", wrapExpressHandler(otpStart));
    router.post("/otp-finish", wrapExpressHandler(otpFinish));
    return router;
  }

  async function otpFinish(req: express.Request<BaseParams>): Promise<ExpressHandlerResponse<{ accessToken: string }>> {
    const token = req.body.token;

    if (typeof token !== 'string') {
      throw new InvalidRequestError("fill in token");
    }

    let decoded: jwt.JwtPayload;
    try {
      decoded = jwt.verify(token, CONFIG_JWT_SECRET(), {
        algorithms: ['HS256'],
        audience: 'svj-email-verifier',
      });
    } catch (e) {
      throw new AccessDeniedError("illegal token");
    }

    const emailAddress = decoded.emailAddress;
    const unitNo = parseInt(decoded.unitNo, 10);

    const account = await storage.loadAccountByEmailAddressAndUnitNo(emailAddress, unitNo);

    if (!account) {
      throw new AccessDeniedError("access denied");
    }

    const accessToken = jwt.sign({
      accountId: account.accountId,
    }, CONFIG_JWT_SECRET(), {
      algorithm: 'HS256',
      expiresIn: CONFIG_SESSION_TIMEOUT_MINUTES() * 60,
      keyid: 'svj-key',
      notBefore: 0,
      audience: 'svj-api'
    });

    return {
      status: 200,
      body: { accessToken },
    };
  }

  async function otpStart(req: express.Request<BaseParams>): Promise<ExpressHandlerResponse<void>> {
    const emailAddress = req.body.emailAddress;
    const unitNo = parseInt(req.body.unitNo, 10);

    if (typeof emailAddress !== 'string' || unitNo < 1 || unitNo > 100) {
      throw new InvalidRequestError("fill in emailAddress and unitNo");
    }

    const account = await storage.loadAccountByEmailAddressAndUnitNo(emailAddress, unitNo);

    if (account) {
      const emailToken = jwt.sign({
        emailAddress,
        unitNo,
      }, CONFIG_JWT_SECRET(), {
        algorithm: 'HS256',
        expiresIn: CONFIG_EMAIL_OTP_TIMEOUT_MINUTES() * 60,
        keyid: 'svj-key',
        notBefore: 0,
        audience: 'svj-email-verifier'
      });

      const href = CONFIG_MAIL_OTP_URL().replace("@", emailToken)

      await mailer.sendMail({
        from: CONFIG_MAIL_FROM(),
        to: emailAddress,
        subject: "SVJ Ukrajinská 14: Přístupový kód",
        text: `Odkaz pro přihlášení do SVJ Ukrajinská 14: ${href}`,
        html: `<p>Klikněte pro <a href="${href}">přihlášení do SVJ Ukrajinská 14</a></p>`
      });
    }

    return {
      status: 201,
      body: undefined,
    };
  }

  async function authenticateSvjUser(req: express.Request, res: express.Response, next: express.NextFunction) {
    const bearerRegex = /^bearer (.*)$/gi;
    const authorizationHeader = bearerRegex.exec(req.header("authorization") || '');
    if (!authorizationHeader) {
      const response = {
        exception: "ILLEGAL_AUTHORIZATION",
      };

      res.status(401).json(response).end();
      return;
    }

    let decoded: jwt.JwtPayload;
    try {
      decoded = jwt.verify(authorizationHeader[1], CONFIG_JWT_SECRET(), {
        algorithms: ['HS256'],
        audience: 'svj-api',
      });
    } catch (e) {
      const response = {
        exception: "ILLEGAL_AUTHORIZATION",
      };

      res.status(401).json(response).end();
      return;
    }

    const account = await storage.loadAccountByAccountId(decoded.accountId);

    if (!account) {
      const response = {
        exception: "UNKNOWN_USER",
      };

      res.status(401).json(response).end();
      return;
    }

    contextSet("accountId", account.accountId);
    req.account = account;

    next();
  }
}
