import { default as cors } from "cors";
import { CronJob } from "cron";
import express from "express";
import { HttpError } from "express-openapi-validator/dist/framework/types";
import pinoHttp from "pino-http";
import {
  CONFIG_BEHIND_PROXY,
  CONFIG_HTTP_MAX_ARRAY_LENGTH,
  CONFIG_HTTP_MAX_STRING_LENGTH,
  CONFIG_LOG_HTTP,
  CONFIG_LOG_HTTP_BODY,
  CONFIG_WEB_ROOT,
} from "./config";
import { createAuthenticationController } from "./controllers/authentication-controller";
import { createPublicController } from "./controllers/public-controller";
import { contextExpress, PARENT_LOGGER } from "./logger";
import { createMailer } from "./mail";
import { Storage } from "./storage";
import { ApplicationError, closeOne, expressMaximumLengthChecker, RoutesConfigurer } from "./utility";

const logger = PARENT_LOGGER.child({ module: "server" });
const httpLogger = PARENT_LOGGER.child({ module: "http" });

export interface SvjApplication {
  app: express.Application;
  close: () => Promise<void>;

  [other: string]: unknown;
}

export async function createApplication({
  storage,
}: {
  storage: Storage;
}): Promise<SvjApplication> {
  const app = express();
  app.set("trust proxy", CONFIG_BEHIND_PROXY());
  app.disable("x-powered-by");
  app.use(express.json({ limit: "1mb" }));
  app.use(express.urlencoded({ extended: false }));
  app.use(expressMaximumLengthChecker(CONFIG_HTTP_MAX_STRING_LENGTH(), CONFIG_HTTP_MAX_ARRAY_LENGTH()));
  app.use(cors({ allowedHeaders: "*", credentials: true }));
  app.use(contextExpress());
  if (CONFIG_LOG_HTTP()) {
    app.use(
      pinoHttp({
        logger: httpLogger,
        useLevel: "trace",
        serializers: { req: requestSerializer() },
      })
    );
  }
  app.use(express.static(CONFIG_WEB_ROOT()))

  const components: unknown[] = [];

  for (const c of [storage]) {
    await component(c);
  }

  const mailer = await component(createMailer());
  const authenticationController = await component(createAuthenticationController(storage, mailer));
  await component(createPublicController(storage, authenticationController));

  const reloadCron = `0 */5 * * * *`;
  await component(new CronJob({ cronTime: reloadCron, onTick: () => storage.refresh() }));

  app.use(handleError);

  return {
    app,
    close,
  };

  async function component<T>(objp: T | Promise<T>): Promise<T> {
    const obj = await objp;
    if (!obj) {
      return obj;
    }
    components.push(obj);
    if (typeof (obj as unknown as RoutesConfigurer).configureRoutes === "function") {
      await (obj as unknown as RoutesConfigurer).configureRoutes(app);
    }
    return obj;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  function handleError(err: unknown, req: express.Request, res: express.Response, _next: express.NextFunction) {
    if (isLoggableError(err)) {
      logger.child({ error: err, requestId: req.id }).error(`Error handling ${req.path}: ${err.message}`);
    }

    const response = constructResponse();
    const status = determineStatus();

    res.status(status).json(response).end();

    function isApplicationError(err: unknown): err is ApplicationError {
      return err instanceof ApplicationError;
    }

    function isLoggableError(err: unknown): err is ApplicationError {
      if (isApplicationError(err) && err.status === 202) {
        return false;
      }
      return true;
    }

    function constructResponse(): unknown {
      if (isApplicationError(err) && typeof err.response === "object") {
        return err.response;
      }
      if (isApplicationError(err)) {
        const error = {
          exception: err.constructor.name,
          error: err.message || "Server error",
        };
        return error;
      } else if (err instanceof HttpError) {
        const error = {
          exception: err.name,
          error: err.message,
        };
        return error;
      } else {
        const error = {
          exception: "SERVER_ERROR",
        };
        return error;
      }
    }

    function determineStatus(): number {
      if (isApplicationError(err) && typeof err.status === "number") {
        return err.status;
      }
      if (err instanceof HttpError) {
        return err.status;
      }
      return 500;
    }
  }

  async function close(): Promise<void> {
    for (const c of components.slice().reverse()) {
      await closeOne(c);
    }
  }
}

function requestSerializer() {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (req0: any) => {
    const req = JSON.parse(JSON.stringify(req0));
    if (CONFIG_LOG_HTTP_BODY() && req0.raw?.headers?.["content-type"] === "application/json" && req0.raw?.body) {
      req.body = typeof req0.raw.body === "string" ? req0.raw.body : JSON.stringify(req0.raw.body);
    }
    if (req.headers?.authorization) {
      delete req.headers.authorization;
    }
    if (req.headers?.["x-api-key"]) {
      delete req.headers["x-api-key"];
    }
    return req;
  };
}
