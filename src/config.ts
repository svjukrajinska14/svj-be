/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import * as changeCase from "change-case";
import * as process from "process";

const versions = Object.keys(process.versions)
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  .map((module) => `${module} ${(process.versions as any)[module]}`)
  .join(", ");

console.error(`NODE ${versions}`);

export const localConfig: Map<string, string> = new Map<string, string>();

export function getStringConfig(name: string, defaultValue: string): string;
export function getStringConfig(name: string, defaultValue?: undefined): string | undefined;
export function getStringConfig(name: string, defaultValue?: string): string | undefined {
  if (localConfig.has(name)) {
    return localConfig.get(name);
  }
  const envValue = "SVJ_" + changeCase.constantCase(name);
  if (envValue in process.env) {
    return process.env[envValue];
  }
  return defaultValue;
}

function getBooleanConfig(name: string, defaultValue: boolean): boolean;
function getBooleanConfig(name: string, defaultValue?: undefined): boolean | undefined;
function getBooleanConfig(name: string, defaultValue?: boolean): boolean | undefined {
  const stringValue = getStringConfig(name);
  if (stringValue === undefined) {
    return defaultValue;
  }
  return ["1", "true"].indexOf(stringValue.toLowerCase()) > -1;
}

function getNumberConfig(name: string, defaultValue: number): number;
function getNumberConfig(name: string, defaultValue?: undefined): number | undefined;
function getNumberConfig(name: string, defaultValue?: number): number | undefined {
  const stringValue = getStringConfig(name);
  if (stringValue === undefined) {
    return defaultValue;
  }
  const numberValue = parseInt(stringValue);
  if (isNaN(numberValue)) {
    return defaultValue;
  }
  return numberValue;
}

export const CONFIG_PORT = () => getNumberConfig("port") || (process.env.PORT && parseInt(process.env.PORT)) || 3000;
export const CONFIG_DEVELOPMENT = () => process.env["NODE_ENV"] !== "production";
export const CONFIG_BEHIND_PROXY = () => getBooleanConfig("behindProxy", false);
export const CONFIG_LOG_HTTP = () => getBooleanConfig("logHttp", false);
export const CONFIG_LOG_HTTP_BODY = () => getBooleanConfig("logHttpBody", false);

export const CONFIG_HTTP_MAX_STRING_LENGTH = () => getNumberConfig("httpMaxStringLength", 1023);
export const CONFIG_HTTP_MAX_ARRAY_LENGTH = () => getNumberConfig("httpMaxArrayLength", 32768);

export const CONFIG_WATERMETER_EXPORTS_PATH = () => getStringConfig("watermeterExportsPath", "/home/jakubs/Work/svj/Exporty")

export const CONFIG_EMAIL_OTP_TIMEOUT_MINUTES = () => getNumberConfig("emailOtpTimeoutMinutes", 10);
export const CONFIG_SESSION_TIMEOUT_MINUTES = () => getNumberConfig("emailOtpTimeoutMinutes", 10);

export const CONFIG_JWT_SECRET = () => getStringConfig("jwtSecret", "changeme")

export const CONFIG_MAIL_HOST = () => getStringConfig("mailHost", "smtp.gmail.com")
export const CONFIG_MAIL_PORT = () => getNumberConfig("mailPort", 587)
export const CONFIG_MAIL_SECURE = () => getBooleanConfig('mailSecure', false)
export const CONFIG_MAIL_USER = () => getStringConfig("mailUser", "svjukrajinska14@gmail.com")
export const CONFIG_MAIL_PASSWORD = () => getStringConfig("mailPassword", "changeme")
export const CONFIG_MAIL_REQUIRE_TLS = () => getBooleanConfig("mailRequireTls", true)
export const CONFIG_MAIL_TLS_REJECT_UNAUTHORIZED = () => getBooleanConfig("mailTlsRejectUnauthorized", true)
export const CONFIG_MAIL_FROM = () => getStringConfig("mailFrom", "svjukrajinska14@gmail.com")

export const CONFIG_MAIL_OTP_URL = () => getStringConfig("mailOtpUrl", "https://svjukrajinska14.cz/#token=@")

export const CONFIG_WEB_ROOT = () => getStringConfig("webRoot", "/home/jakubs/Work/svj/DEV/svj-fe/dist")
