import { createNamespace } from "cls-hooked";
import { IncomingMessage, NextFunction } from "connect";
import * as http from "http";
import * as os from "os";
import pino from "pino";
import * as process from "process";
import { CONFIG_DEVELOPMENT } from "./config";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const PARENT_LOGGER = pino({ level: "trace", prettyPrint: CONFIG_DEVELOPMENT(), mixin: loggerMixin } as any);

const PARENT_CLS = createNamespace("svj.logger");

export interface ContextValues {
  request?: unknown;
  response?: unknown;
  accountId?: string;
  requestId?: string;

  [name: string]: unknown;
}

export function contextRun(fn: () => void): void {
  PARENT_CLS.run(() => {
    PARENT_CLS.set("log", {});
    fn();
  });
}

export async function contextRunPromise(fn: () => Promise<void>): Promise<void> {
  return await PARENT_CLS.runPromise(async () => {
    PARENT_CLS.set("log", {});
    return await fn();
  });
}

export function contextSet(key: string, value: unknown): void {
  PARENT_CLS.set("log", { ...PARENT_CLS.get("log"), [key]: value });
}

export function contextSetAll(kv: { [name: string]: unknown }): void {
  PARENT_CLS.set("log", { ...PARENT_CLS.get("log"), ...kv });
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function contextBind<F extends Function>(fn: F): F {
  return PARENT_CLS.bind(fn);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function contextOpenApi(): (req: any, res: http.ServerResponse, next: NextFunction) => void {
  return (req, _res, next) => {
    const operationId = req?.openapi?.schema?.operationId;
    if (operationId) {
      contextSet("operationId", operationId);
    }
    next();
  };
}

function loggerMixin(): Record<string, unknown> {
  const values: ContextValues = { ...PARENT_CLS.get("log") };
  return values ?? {};
}

export function contextExpress(): (req: IncomingMessage, res: http.ServerResponse, next: NextFunction) => void {
  let i = 1;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (req: any, res: http.ServerResponse, next: NextFunction) => {
    contextRunPromise(async () => {
      if (!req.id) {
        req.id = `http.${os.hostname()}.${process.pid}.${i++}`;
      }
      res.setHeader("x-request-id", req.id);

      contextSet("requestId", req.id);

      const p = new Promise((resolve, reject) => {
        res.on("finish", resolve);
        res.on("error", reject);
      });

      next();

      await p;
    });
  };
}
